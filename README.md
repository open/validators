# Validators

Package with multiple complex validation entities (only CPF & CNPJ now)

Usage:

```go
package main

import (
	"go.digitalcircle.com.br/open/validators/cpf"
	"log"
)

func main() {
	acpf := "401.852.780-27"
	if cpf.Check(acpf) {
		log.Printf("CPF OK: %s", cpf.Format(acpf))
	}
}

```