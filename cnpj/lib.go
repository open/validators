package cnpj

import (
	"strconv"
	"unicode"
)

//Gets a string and formats is as CPF
func Format(s string) string {
	raw := Strip(s)
	ret := raw[0:2] + "." + raw[2:5] + "." + raw[5:8] + "/" + raw[8:12] + "-" + raw[12:14]
	return ret

}

//Removes all non-digit symbols
func Strip(s string) string {
	ret := ""
	for _, v := range s {
		if unicode.IsDigit(v) {
			ret = ret + string(v)
		}
	}
	return ret
}

//Checks if verification digit are respected
func Check(s string) bool {
	raw := Strip(s)
	nums := make([]int, 0)
	for _, v := range raw {
		i, _ := strconv.Atoi(string(v))
		nums = append(nums, i)
	}

	d1 := 0
	mul := 5
	for _, v := range nums[0 : len(nums)-2] {
		d1 = d1 + (v * mul)
		mul--
		if mul == 1 {
			mul = 9
		}
	}
	mod1 := ((d1) % 11)
	if mod1 < 2 {
		mod1 = 0
	} else {
		mod1 = 11 - mod1
	}

	d2 := 0
	mul = 6
	for _, v := range nums[0 : len(nums)-2] {
		d2 = d2 + (v * mul)
		mul--
		if mul == 1 {
			mul = 9
		}
	}
	d2 = d2 + (mod1 * mul)
	mod2 := (d2) % 11
	if mod2 < 2 {
		mod2 = 0
	} else {
		mod2 = 11 - mod2
	}
	return mod1 == nums[len(nums)-2] && mod2 == nums[len(nums)-1]
}
