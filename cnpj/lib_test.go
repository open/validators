package cnpj

import (
	"log"
	"testing"
)

func TestFormat(t *testing.T) {
	log.Printf(Format("03794020685"))
}

func TestCheck(t *testing.T) {
	for _, v := range []string{
		"60.119.200/0001-90",
		"77.791.482/0001-70",
		"78.803.323/0001-00",
		"92.632.766/0001-07",
		"42.756.293/0001-81",
		"59.663.919/0001-08",
	} {
		ret := Check(v)
		log.Printf("%s=>%v", v, ret)
	}

}
