package cpf

import (
	"log"
	"testing"
)

func TestFormat(t *testing.T) {
	log.Printf(Format("03794020685"))
}

func TestCheck(t *testing.T) {
	for _, v := range []string{
		"377.100.400-47",
		"377.100.400-46",
		"845.846.910-37",
		"845.846.910-17",
		"39412818602",
		"768.974.608-00",
	} {
		ret := Check(v)
		log.Printf("%s=>%v", v, ret)
	}

}
